#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#include "matrix.h"
#include "jacobi.h"
#include "vector.h"
#include "thread.h"

#define EXECUTE_N_TIMES 		10 	//mude essa flag para executar mais iteracoes do mesmo codigo
#define DEFAULT_THREAD_N 		4	//define o numero de threads

//GLOBAL
	unsigned int J_ORDER;		//ordem da matriz
	unsigned int J_ROW_TEST;	//fila de teste
	double J_ERROR;				//erro permitido
	unsigned int J_ITE_MAX;		//maximo de iteracoes
	unsigned int N_THREADS;		//numero de threads

	int J_INTERATION=0;			//iteracoes realizadas

/**
 * Essa funcao le da entrada padrao todos os argumentos.
 * A entrada precisa ser bem formatada para funcionar
 * @param A Matriz para guardar os dados
 * @param B Array para guardar B
 */
void readArgs(MATRIX **A, double **B){
	
	scanf("%u", &J_ORDER);
	scanf("%u", &J_ROW_TEST);
	scanf("%lf", &J_ERROR);
	scanf("%u", &J_ITE_MAX);

	(*A) = allocMatrix(J_ORDER);
	readMatrix(*A);

	(*B) = readVector(J_ORDER);
}

/**
 * Libera um par de argumetos
 * @param A Matriz
 * @param B Array
 */
void freeArgs(MATRIX *A, double *B){
	freeMatrix(A);
	freeVector(B);
}

/**
 * Criterio de parada do metodo
 * @param  Xold A ultima iteracao
 * @param  Xnew Iteracao atual
 * @return      retorna 1 se falhar na parada
 *              retorna 0 se deve parar
 */
int stopCriteria(double *Xold, double *Xnew){

	if(J_INTERATION	 >= J_ITE_MAX);
	else if(evaluateErrorMT(Xold, Xnew, J_ORDER, J_ERROR));
	else return 1;

	return 0;
}

/**
 * testa a linha e retorna o resultado
 * @param  originalA linha de teste
 * @param  X         matriz X para multiplicar com A
 * @return           resultado final da avaliacao
 */
double rowTest(double *originalA, double *X){

	int i;
	double result = 0;

	for(i=0; i < J_ORDER; i++)
		result += originalA[i]*X[i];

	return result;
}

/**
 * Funcao para definir o numero de threads do sistema
 * @return retorna o numero de nucleos fisicos
 */
unsigned int defineThreads(){

	int cores = sysconf(_SC_NPROCESSORS_ONLN);

	return cores;
}

int main(){

	MATRIX *A; 				//matriz para iteracoes
	MATRIX *trueA = NULL;	//matriz original
	
	double *B;				//matriz para iteracoes
	double *trueB = NULL;	//matriz original

	double *X = NULL;		//matriz para iteracao antiga
	double *newX = NULL;	//matriz para iteracao atual

	clock_t begin, end;		//tempo inicio/fim
	double time = 0;		//diferenca

	THREAD_ARGS *th_args; 	//argumento para threads

	readArgs(&A, &B);		//le os argumentos da STDIN
	trueB = copyVector(trueB, B, J_ORDER); //copia o vetor lido para um auxiliar
	trueA = copyMatrix(trueA, A); //copia o vetor lido para um auxiliar

	N_THREADS = DEFAULT_THREAD_N;		//numero de threads para processar

	if(N_THREADS > J_ORDER){ //pequena checagem de erro do programa
		printf("BAD THREAD ARG\n");
		return 1;
	}

	th_args = getThreadArgs(N_THREADS, A, B, X); //cria a estrutura de argumentos para threads

	operation_divDiag_MT(th_args, N_THREADS); //divide as diagonais

	int i;
	for(i=0; i < EXECUTE_N_TIMES; i++){ //realiza o programa N vezes

		begin = clock(); //marca inicio

		X = copyVector(X, B, J_ORDER);	//X eh incialmente B
		setNewX(X, th_args, N_THREADS);	//atualiza X nos argumentos das threads

		do{ //loop principal do progrmaa
			newX = interation_Jacobi_MT(th_args, N_THREADS); //novo X eh gerado

			if(i==0) //contar o numero de iteracoes apenas uma vez
				J_INTERATION++;

			if(!stopCriteria(X, newX)){ //testa o criterio de parada
				X = freeVector(X); //libera  X
				break; //para o loop
			}

			freeVector(X); //X nao eh mais necessario
			X = newX; //X eh atualizado
			setNewX(X, th_args, N_THREADS); //atualiza argumentos das threads

		}while(1);

		end = clock(); //recebe o fim do tempo
		time += (double) (end - begin)/CLOCKS_PER_SEC; //adiciona ao total do tempo
	}

	//eh necessario dividir o tempo total pelo numero de threads
	//alem disso, para a media, dividimos por N execucoes
	printf("AVG. TIME OF EACH EXECUTION OF %d TOTAL: %.2lfs\n", EXECUTE_N_TIMES, time/(N_THREADS*EXECUTE_N_TIMES));

	//imprime o numero de iteracoes
	printf("INTERATIONS: %d\n", J_INTERATION);

	//imprime a linha de teste
	printf("ROWTEST:%d => [%.5lf] =? [%.5lf]\n", J_ROW_TEST, rowTest(trueA->matrix[J_ROW_TEST], newX), trueB[J_ROW_TEST]);

	//liberando tudo
	destroyThreadArgs(th_args);
	freeArgs(A, B);
	freeArgs(trueA, trueB);

	freeVector(X);
	freeVector(newX);

	return 0;
}
