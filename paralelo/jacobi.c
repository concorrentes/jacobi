#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "matrix.h"
#include "jacobi.h"
#include "vector.h"

//funcao que prepara as matrizes para iterar Jacobi
void operation_divDiag(MATRIX *A, double *B, unsigned int start, unsigned int size){

	int dim = A->dim;
	double target;
	int i, j;

	for(i=start; i < start+size; i++){
		
		target = A->matrix[i][i]; //recebendo diagonal

		B[i] /= target;	//dividindo celula de B pela diagonal

		for(j=0; j < dim; j++){
			if(i==j){
				A->matrix[i][j] = 0; //se for a diagonal recebe 0
			} else A->matrix[i][j] /= target; //se nao for recebe a divisao pela diagonal
		}
	}
}

/**
 * iteracao de jacobi
 * @param  A     Matriz A
 * @param  B     Matriz B
 * @param  X     Matriz X
 * @param  start indice incial
 * @param  size  tamanho do bloco. Ultimo indice = start + size
 * @return       Novo X
 */
double* interation_Jacobi(MATRIX *A, double *B, double *X, unsigned int start, unsigned int size){

	int dim = A->dim; //dimensao da matriz

	double *Xnew = allocVector(size); //alocando o novo X
	int i, j;

	for(i=start; i < start+size; i++){ //iterando
		for(j=0; j < dim; j++){
			Xnew[i-start] += -(A->matrix[i][j]*X[j]); //metodo em si
		}

		Xnew[i-start] += B[i]; //finalizando
	}

	return Xnew;
}

