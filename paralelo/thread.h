#ifndef THREAD_H
#define THREAD_H

typedef struct thread_args{

	pthread_t thread;
	MATRIX *A;
	double *B;
	double *X;
	unsigned int start;
	unsigned int size;

}THREAD_ARGS;

typedef struct errorArgs{
	double *Xold;
	double *Xnew;
	unsigned int size;
}ERROR_ARGS;

THREAD_ARGS *getThreadArgs(int n_threads, MATRIX *A, double *B, double *X);
THREAD_ARGS *destroyThreadArgs(THREAD_ARGS* args);
void setNewX(double *newX, THREAD_ARGS *args, unsigned int n_threads);

void *interation_Jacobi_MT_call(void *argv);
double* interation_Jacobi_MT(THREAD_ARGS *args, unsigned int n_threads);

void* operation_divDiag_MT_call(void *argv);
void operation_divDiag_MT(THREAD_ARGS *args, unsigned int n_threads);

int evaluateErrorMT(double *Xold, double *Xnew, unsigned int size, double J_ERROR);

#endif