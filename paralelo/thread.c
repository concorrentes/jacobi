#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "vector.h"
#include "matrix.h"
#include "vector.h"
#include "thread.h"
#include "jacobi.h"

int getStart(int array_size, int n_elements, int i){
	return (array_size/n_elements)*i;
}

int getSize(int array_size, int n_elements, int i){
	if(i == n_elements-1) 
		return (array_size/n_elements)+(array_size%n_elements);
	return (array_size/n_elements);
}

THREAD_ARGS *getThreadArgs(int n_threads, MATRIX *A, double *B, double *X){
	
	int i;
	THREAD_ARGS *args = (THREAD_ARGS*) malloc(sizeof(THREAD_ARGS)*n_threads);

	for(i=0; i < n_threads; i++){
		args[i].A = A;
		args[i].B = B;
		args[i].X = X;
		args[i].start = getStart(A->dim, n_threads, i); 
		args[i].size = getSize(A->dim, n_threads, i);
	}

	return args;
}
void setNewX(double *newX, THREAD_ARGS *args, unsigned int n_threads){
	int i;

	for(i=0; i < n_threads; i++)
		args[i].X = newX;
}

THREAD_ARGS *destroyThreadArgs(THREAD_ARGS* args){
	
	if (args != NULL)
		free(args);

	return NULL;
}

void *interation_Jacobi_MT_call(void *argv){

	THREAD_ARGS *args = (THREAD_ARGS*) argv;
	double *buffer = interation_Jacobi(args->A, args->B, args->X, args->start, args->size);

	return (void*) buffer;
}

double* interation_Jacobi_MT(THREAD_ARGS *args, unsigned int n_threads){

	double *buffer;
	double *final_vector = allocVector(args->A->dim);
	int i;

	for(i=0; i < n_threads; i++){
		pthread_create(&(args[i].thread), NULL, &interation_Jacobi_MT_call, &(args[i]));
	}
	for(i=0; i < n_threads; i++){
		pthread_join(args[i].thread, (void*) &buffer);
		appendVector(final_vector, buffer, args[i].start, args[i].size);
		free(buffer);
	}

	return final_vector;
}

void* operation_divDiag_MT_call(void *argv){
	THREAD_ARGS *args = (THREAD_ARGS*) argv;

	operation_divDiag(args->A, args->B, args->start, args->size);

	return NULL;
}

void operation_divDiag_MT(THREAD_ARGS *args, unsigned int n_threads){
	
	int i;

	for(i=0; i < n_threads; i++)
		pthread_create(&(args[i].thread), NULL, &operation_divDiag_MT_call, &(args[i]));

	for(i=0; i < n_threads; i++)
		pthread_join(args[i].thread, NULL);
}

void* evaluateErrorDOWN(void *argv){

	ERROR_ARGS *arg = (ERROR_ARGS*) argv;

	double *max_xnew = (double*) malloc(sizeof(double));
	double *Xnew = arg->Xnew;
	int i;

	(*max_xnew) = fabs(Xnew[0]);

	for(i=1; i < arg->size; i++){
		if((*max_xnew) < fabs(Xnew[i])){
			(*max_xnew) = fabs(Xnew[i]);
		}
	}

	return (void*) max_xnew;
}

void* evaluateErrorUP(void *argv){
	
	ERROR_ARGS *arg = (ERROR_ARGS*) argv;

	double *Xold = arg->Xold;
	double *Xnew = arg->Xnew;
	double *max_diff = (double*) malloc(sizeof(double)); 
	(*max_diff) = fabs(Xnew[0] - Xold[0]);
	int i;

	for(i=1; i < arg->size; i++){
		if( (*max_diff) < fabs(Xnew[i] - Xold[i])){
			(*max_diff) = fabs(Xnew[i] - Xold[i]);
		}
	}

	return (void*) max_diff;
}

int evaluateErrorMT(double *Xold, double *Xnew, unsigned int size, double J_ERROR){

	ERROR_ARGS arg;
	arg.Xold = Xold;
	arg.Xnew = Xnew;
	arg.size = size;

	pthread_t thread1, thread2;
	double *up;
	double *down;
	int flag = 0;

	pthread_create(&thread1, NULL, &evaluateErrorUP, &arg);
	pthread_create(&thread2, NULL, &evaluateErrorDOWN, &arg);

	pthread_join(thread1, (void*) &up);
	pthread_join(thread2, (void*) &down);
	
	if( (*up) / (*down) <= J_ERROR)
		flag = 1;
	
	free(down);
	free(up);
	return flag;
}

