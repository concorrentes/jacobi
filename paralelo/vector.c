#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "vector.h"

VTYPE *allocVector(unsigned int n){
	VTYPE *buffer;
	
	buffer = (VTYPE*) calloc(n,sizeof(VTYPE));
	return buffer;
}

VTYPE *readVector(unsigned int n){
	
	int i;	
	VTYPE *buffer = allocVector(n);

	for(i=0; i<n; i++){
		scanf("%lf", &buffer[i]);
	}

	return buffer;
}

void printVector(VTYPE *vector, unsigned int n){
	int i;

	for(i=0; i<n; i++)
		printf("%.5lf ", vector[i]);
	
	printf("\n");
}

VTYPE* freeVector(VTYPE *vector){
	if(vector != NULL)
		free(vector);
	return NULL;
}

VTYPE *copyVector(VTYPE *destination, VTYPE *source, unsigned int n){

	if(destination == NULL)
		destination = allocVector(n);
	memcpy(destination, source, n*sizeof(VTYPE));

	return destination;
}

void appendVector(VTYPE *destination, VTYPE *source, unsigned int start, unsigned int size){

	memcpy(&(destination[start]), source, size*sizeof(VTYPE));
}