#ifndef JACOBY_H
#define JACOBY_H


/**
 * funcao que prepara as matrizes para iterar Jacobi
 * @param A     Matriz A
 * @param B     Matriz B
 * @param start tamanho do bloco. Ultimo indice = start + size
 * @param size  ate qual indice deve fazer a divisao
 */
void operation_divDiag(MATRIX *A, double *B, unsigned int start, unsigned int size);

/**
 * iteracao de jacobi
 * @param  A     Matriz A
 * @param  B     Matriz B
 * @param  X     Matriz X
 * @param  start indice incial
 * @param  size  tamanho do bloco. Ultimo indice = start + size
 * @return       [description]
 */
double* interation_Jacobi(MATRIX *A, double *B, double *X, unsigned int start, unsigned int size);

#endif
