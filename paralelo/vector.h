#ifndef VECTOR_H
#define VECTOR_H

typedef double VTYPE;

VTYPE *allocVector(unsigned int n);
VTYPE *readVector(unsigned int n);
void printVector(VTYPE *vector, unsigned int n);
VTYPE *copyVector(VTYPE *destination, VTYPE *source, unsigned int n);
VTYPE* freeVector(VTYPE *vector);
void appendVector(VTYPE *destination, VTYPE *source, unsigned int start, unsigned int size);


#endif
