#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "vector.h"

/**
 * Aloca vetor
 * @param  n tamanho do vetor
 * @return   vetor
 */
double *allocVector(unsigned int n){
	double *buffer;
	
	buffer = (double*) calloc(n,sizeof(double));
	return buffer;
}


/**
 * Libera um vetor
 * @param  vector vetor para liberar
 * @return        NULL
 */
double* freeVector(double *vector){
	if(vector != NULL)
		free(vector);
	return NULL;
}

/**
 * imprime um leitor na saida padrao
 * @param vector vetor
 * @param n      tamanho
 */
double *readVector(unsigned int n){
	
	int i;	
	double *buffer = allocVector(n);

	for(i=0; i<n; i++){
		scanf("%lf", &buffer[i]);
	}

	return buffer;
}

/**
 * imprime um leitor na saida padrao
 * @param vector vetor
 * @param n      tamanho
 */
void printVector(double *vector, unsigned int n){
	int i;

	for(i=0; i<n; i++)
		printf("%.5lf ", vector[i]);
	
	printf("\n");
}


/**
 * Copia um vetor para outro
 * @param  destination vetor destino
 * @param  source      vetor fonte
 * @param  n           tamanho
 * @return             retorna destino
 */
double *copyVector(double *destination, double *source, unsigned int n){

	if(destination == NULL)
		destination = allocVector(n);
	memcpy(destination, source, n*sizeof(double));

	return destination;
}
