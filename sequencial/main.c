#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "matrix.h"
#include "jacobi.h"
#include "vector.h"

#define EXECUTE_N_TIMES 		10 	//mude essa flag para executar mais iteracoes do mesmo codigo


//GLOBAL
	unsigned int J_ORDER;		//ordem da matriz
	unsigned int J_ROW_TEST;	//fila de teste
	double J_ERROR;				//erro permitido
	unsigned int J_ITE_MAX;		//maximo de iteracoes
	unsigned int N_THREADS;		//numero de threads

	int J_INTERATION=0;			//iteracoes realizadas

/**
 * Essa funcao le da entrada padrao todos os argumentos.
 * A entrada precisa ser bem formatada para funcionar
 * @param A Matriz para guardar os dados
 * @param B Array para guardar B
 */
void readArgs(MATRIX **A, double **B){
	
	scanf("%u", &J_ORDER);
	scanf("%u", &J_ROW_TEST);
	scanf("%lf", &J_ERROR);
	scanf("%u", &J_ITE_MAX);

	(*A) = allocMatrix(J_ORDER);
	readMatrix(*A);

	(*B) = readVector(J_ORDER);
}

/**
 * Libera um par de argumetos
 * @param A Matriz
 * @param B Array
 */
void freeArgs(MATRIX *A, double *B){
	freeMatrix(A);
	freeVector(B);
}

/**
 * Avalia o erro
 * @param  Xold iteracao antiga
 * @param  Xnew iteracao atual
 * @return      retorna 0 se o erro nao ultrapassou e 1 se ultrapassou
 */
int evaluateError(double *Xold, double *Xnew){

	double max_diff = fabs(Xnew[0] - Xold[0]);
	double max_xnew = fabs(Xnew[0]);
	int i;

	for(i=1; i < J_ORDER; i++){
		if(max_diff < fabs(Xnew[i] - Xold[i])){
			max_diff = fabs(Xnew[i] - Xold[i]);
		}
	}


	for(i=1; i < J_ORDER; i++){
		if(max_xnew < fabs(Xnew[i])){
			max_xnew = fabs(Xnew[i]);
		}
	}

	if( max_diff / max_xnew <= J_ERROR){
		return 1;
	}

	return 0;
}

/**
 * Criterio de parada do metodo
 * @param  Xold A ultima iteracao
 * @param  Xnew Iteracao atual
 * @return      retorna 1 se falhar na parada
 *              retorna 0 se deve parar
 */
int stopCriteria(double *Xold, double *Xnew){

	if(J_INTERATION	 >= J_ITE_MAX);
	else if(evaluateError(Xold, Xnew));
	else return 1;

	return 0;
}

/**
 * Testa o resultado
 * @param  originalA linha da matriz A
 * @param  X         matriz X
 * @return           valor aproximado
 */
double rowTest(double *originalA, double *X){

	int i;
	double result = 0;

	for(i=0; i < J_ORDER; i++)
		result += originalA[i]*X[i];

	return result;
}


int main(){

	MATRIX *A; 				//matriz para iteracoes
	MATRIX *trueA = NULL;	//matriz original
	
	double *B;				//matriz para iteracoes
	double *trueB = NULL;	//matriz original

	double *X = NULL;		//matriz para iteracao antiga
	double *newX = NULL;	//matriz para iteracao atual

	clock_t begin, end;		//tempo inicio/fim
	double time = 0;		//diferenca

	readArgs(&A, &B);		//le os argumentos de stdin

	trueB = copyVector(trueB, B, J_ORDER);	//copia a matriz original para auxiliar
	trueA = copyMatrix(trueA, A);			//copia a matriz original para auxiliar

	operation_divDiag(A, B);		//divide diagonal
	
	int i;
	for(i=0; i < EXECUTE_N_TIMES; i++){	//executa o numero de vezes especificado

		X = copyVector(X, B, J_ORDER);

		begin = clock();

		do{
			newX = interation_Jacobi(A, B, X);
			
			if(i==0) //so conta a interacao uma vez
				J_INTERATION++;
			
			if(!stopCriteria(X, newX)){
				X = freeVector(X);
				break;
			}

			freeVector(X);
			X = newX;
		
		}while(1);

		end = clock();
		time += (double) (end - begin)/CLOCKS_PER_SEC;
	}

	printf("AVG. TIME OF EACH EXECUTION OF %d TOTAL: %.2lfs\n", EXECUTE_N_TIMES, time/EXECUTE_N_TIMES);

	printf("INTERATIONS: %d\n", J_INTERATION);

	printf("ROWTEST:%d => [%.5lf] =? [%.5lf]\n", J_ROW_TEST, rowTest(trueA->matrix[J_ROW_TEST], newX), trueB[J_ROW_TEST]);

	freeArgs(A, B);
	freeArgs(trueA, trueB);

	freeVector(newX);
	
	return 0;
}

