#ifndef JACOBY_H
#define JACOBY_H

/**
 * Divide a diagonal. Prepara para iterar Jacobi
 * @param A Matriz quadrada
 * @param B Matriz coluna
 */
void operation_divDiag(MATRIX *A, double *B);

/**
 * Iteracao de Jacobi
 * @param  A matriz quadrada A
 * @param  B matriz B
 * @param  X matrix X atual
 * @return   retorna um novo X
 */
double* interation_Jacobi(MATRIX *A, double *B, double *X);

#endif
