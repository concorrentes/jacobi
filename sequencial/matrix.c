#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "matrix.h"

/**
 * Aloca uma nova matriz
 * @param  dim dimensao da matriz
 * @return     retorna a nova matriz
 */
MATRIX *allocMatrix(unsigned int dim){
	
	int i;

	MATRIX *matrix = (MATRIX*) malloc(sizeof(MATRIX));
	matrix->matrix = (double**) malloc(sizeof(double*)*dim);
	matrix->dim = dim;

	for(i=0; i < dim; i++){
		matrix->matrix[i] = malloc(sizeof(double)*dim);
	}

	return matrix;
}

/**
 * Libera uma matriz
 * @param  matrix matriz ou NULL
 * @return        NULL
 */
MATRIX *freeMatrix(MATRIX *matrix){
	
	int i;

	if(matrix != NULL){

		for(i=0; i < matrix->dim; i++)
			free(matrix->matrix[i]);
	
		free(matrix->matrix);

		free(matrix);
	}

	return NULL;
}

/**
 * Imprime uma matriz. bom para debug
 * @param matrix Matriz para imprimir
 */
void printMatrix(MATRIX *matrix){
	
	int i, j;

	for(i = 0; i < matrix->dim; i++){
		for(j = 0; j < matrix->dim; j++){
			printf("%.5f\t", matrix->matrix[i][j]);
		}
		printf("\n");
	}
}

/**
 * Le uma matrix da entrada padrao
 * @param matrix matriz ja alocada!
 */
void readMatrix(MATRIX *matrix){
	
	int i, j;

	for (i = 0; i < matrix->dim; i++){
		for (j = 0; j < matrix->dim; j++){
			scanf("%lf", &(matrix->matrix[i][j]));
		}
	}
}

/**
 * Funcao que copia uma matriz 
 * @param  destination destino
 * @param  source      fonte
 * @return             destino
 */
MATRIX *copyMatrix(MATRIX *destination, MATRIX *source){
	int i;
	if(destination == NULL)
		destination = allocMatrix(source->dim); //aloca uma matriz

	for(i=0; i < source->dim; i++)
		memcpy(destination->matrix[i], source->matrix[i], source->dim*sizeof(double)); //copia do segmento de memoria
	return destination;
}
