#ifndef MATRIX_H
#define MATRIX_H


typedef struct matrix{
	unsigned int dim;	//dimensao
	double **matrix;	//matriz
}MATRIX;

/**
 * Aloca uma nova matriz
 * @param  dim dimensao da matriz
 * @return     retorna a nova matriz
 */
MATRIX *allocMatrix(unsigned int dim);


/**
 * Libera uma matriz
 * @param  matrix matriz ou NULL
 * @return        NULL
 */
MATRIX *freeMatrix(MATRIX *matrix);

/**
 * Imprime uma matriz. bom para debug
 * @param matrix Matriz para imprimir
 */
void printMatrix(MATRIX *matrix);

/**
 * Le uma matrix da entrada padrao
 * @param matrix matriz ja alocada!
 */
void readMatrix(MATRIX *matrix);


/**
 * Funcao que copia uma matriz 
 * @param  destination destino
 * @param  source      fonte
 * @return             destino
 */
MATRIX *copyMatrix(MATRIX *destination, MATRIX *source);

#endif
