#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"
#include "jacobi.h"
#include "vector.h"

void operation_divDiag(MATRIX *A, double *B){

	int dim = A->dim;
	double target;
	int i, j;

	for(i=0; i < dim; i++){
		
		target = A->matrix[i][i];

		B[i] /= target;

		for(j=0; j < dim; j++){
			if(i==j){
				A->matrix[i][j] = 0;
			} else A->matrix[i][j] /= target;
		}
	}
}

double* interation_Jacobi(MATRIX *A, double *B, double *X){

	int dim = A->dim;
	double *Xnew = allocVector(dim);
	int i, j;

	for(i=0; i < dim; i++){
		
		for(j=0; j < dim; j++){
			Xnew[i] += -(A->matrix[i][j]*X[j]);
		}

		Xnew[i] += B[i];
	}


	return Xnew;
}

