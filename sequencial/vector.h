#ifndef VECTOR_H
#define VECTOR_H

/**
 * Aloca vetor
 * @param  n tamanho do vetor
 * @return   vetor
 */
double *allocVector(unsigned int n);

/**
 * le um vetor de tamanho n da entrada padrao
 * @param  n 
 * @return   retorna o vetor lido
 */
double *readVector(unsigned int n);

/**
 * imprime um leitor na saida padrao
 * @param vector vetor
 * @param n      tamanho
 */
void printVector(double *vector, unsigned int n);

/**
 * Copia um vetor para outro
 * @param  destination vetor destino
 * @param  source      vetor fonte
 * @param  n           tamanho
 * @return             retorna destino
 */
double *copyVector(double *destination, double *source, unsigned int n);

/**
 * Libera um vetor
 * @param  vector vetor para liberar
 * @return        NULL
 */
double* freeVector(double *vector);

#endif
