#!/bin/bash
#
#SCRIPT TO COMPILE EVERYTHING
#Recursive Makefile is a bad thing...

SUBDIRS=("sequencial" "paralelo")

function compile(){
	echo "[@SCRIPT] DIR: $1"
	cd $1
	make
	cd ..
}


function rebuild(){
	cd $1
	make rebuild
	cd ..
}

function checkStringArray(){

	for i in "${SUBDIRS[@]}"; do
		if [[ $1 == i ]]; then
			return 1
		fi
	done

	return 0
}

if [ "$#" -eq  0 ]; then

	for i in "${SUBDIRS[@]}"; do
		compile $i
	done

	else echo "[SCRIPT] USAGE: ./compile.sh"
fi
