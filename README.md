## Synopsis

	Esse é o primeiro trabalho da disciplina de programação concorrente SSC0143.

## Compile

	$./compile.sh

	> Procura pelo diretórios especificados e compila com o comando "make"

## Run
	
	$./run.sh
	
	> Isso vai gerar saidas no formato: <*.in><programa_usado>.out
	> Para adicionar uma matriz de entrada apenas adicione um arquivo .in no diretório principal da aplicação.


##FAQ

	1) Como faço para adicionar uma matriz aos casos de teste?
		Crie um arquivo com a terminação .in

		O conteúdo deve seguir o seguinte exemplo:

		<inteiro  >			//ordem da matriz
		<inteiro  >			//fila para ser avaliada
		<float    >			//erro
		<inteiro  >			//numero maximo de interações
		<MATRIZ A > 		//primeira linha da matriz A
		...					//n linhas da matriz A
		<MATRIZ B> 			//primeira linha da matriz B
		...					//n linha da matriz B


		Ou ainda:
		3 -> ordem da matriz (J_ORDER)
		2 -> fila para ser avaliada (J_ROW_TEST)
		0.001 -> erro permitido (J_ERROR)
		20000 -> número máximo de iterações (J_ITE_MAX)
		4 2 1 -|
		1 3 1 -|-> matriz A (MA)
		2 3 6 -|
		7   -|
		-8  -|-> matriz B (MB)
		6   -|


	2) Quero rodar/compilar apenas um dos subprogramas (paralelo ou sequencial). Como faço?
		Entre no script correspondente e apague o nome do sbudiretório que você não quer compilar/rodar.

	3) Como especificar o número de threads que o paralelo vai utilizar?
		Você deve alterar um número no arquivo main.c e compilar
		Atualmente essa flag é: #define DEFAULT_THREAD_N  <inteiro+>

	4) Como faço para alterar o número de vezes que o programa roda para fazer o benchmark de tempo?
		Você deve alterar o seguinte define: #define EXECUTE_N_TIMES


## Contributors
	Universidade de São Paulo ICMC
	Oscar Cardoso Lima Neto		8065863		oscarneto@usp.br
	Paulo Guarnier De Mitri		3555888 	paulo.mitri@usp.br

## License

Copyright (c) 2015

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

