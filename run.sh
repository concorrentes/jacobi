#!/bin/bash
#

SUBDIRS=("sequencial" "paralelo")
ARRAY=($(ls *in))

array=(*in)

for dir in "${SUBDIRS[@]}"; do
	cd $dir 
	for input in "${array[@]}"; do
		echo "Processing: $input under /$dir"
 		make run < ../$input > ../"$input""$dir".out
 		echo "Done."
	done
	cd ..
done


